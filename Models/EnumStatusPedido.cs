using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;

namespace tech_test_payment_api.Models
{
    public enum EStatusPedido
    {
        [Description("Aguardando Pagamento")]
        AguardandoPagamento,
        [Description("Pagamento Aprovado")]
        PagamentoAprovado,
        [Description("Enviado para Transportadora")]
        EnviadoParaTransportadora,
        [Description("Entregue")]
        Entregue,
        [Description("Cancelado")]
        Cancelado

    }
}