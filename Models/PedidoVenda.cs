using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Models;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;


namespace tech_test_payment_api.Models
{
    
    public class PedidoVenda
    {
        public PedidoVenda(Vendedor vendedor)
        {
            Vendedor = vendedor;
            DataVenda = DateTime.Now;
            StatusPedido = EStatusPedido.AguardandoPagamento;
            Produtos = new List<Produto>();
        }
        public PedidoVenda(Vendedor vendedor, DateTime date, EStatusPedido status, List<Produto> produtos)
        {
            Vendedor = vendedor;
            DataVenda = date;
            StatusPedido = status;
            Produtos = produtos;
        }
        public PedidoVenda()
        {
            
        }
        public int Id { get; set; }
        public Vendedor Vendedor { get; set; }
        public IList<Produto> Produtos { get; set; }
        public DateTime DataVenda { get; set; }
        public EStatusPedido StatusPedido { get; set; }

       

    }
}