using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    public class Vendedor
    {

        public Vendedor(string cpf, string nome, string email, string telefone)
        {
            Cpf = cpf;
            Nome = nome;
            Email = email;
            Telefone = telefone;
        }
        public int Id { get; set; }
        public string Cpf { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }

      
    }
}