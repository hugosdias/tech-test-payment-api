using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace tech_test_payment_api.Models
{
     
    public class Produto
    {
        
        public virtual int Id { get; set; }
        public string NomeProduto { get; set; }
        public decimal PrecoProduto { get; set; }
        
    }
}