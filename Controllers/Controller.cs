using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;


namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    
    public class PedidoVendaController : ControllerBase
    {
        private readonly VendasContext _context;
        public PedidoVendaController(VendasContext context)
        {
            _context = context;
        }
        [HttpPost]
       public IActionResult RegistrarVenda(PedidoVenda pedidoVenda)
       {
            
            _context.Add(pedidoVenda);
            _context.SaveChanges();
            return CreatedAtAction(nameof(ObterVendaPorId), new { idVenda = pedidoVenda }, pedidoVenda);

        }
        [HttpGet("{idVenda}")]
        public IActionResult ObterVendaPorId(int idVenda)
        {
            var venda = _context.PedidoVendas.Find(idVenda);
            if(venda == null)
                return NotFound();
            return Ok(venda);
        }

        [HttpPatch("{AtualizarStatusPedido}")]
        public IActionResult AtualizarPedidoVenda(int idVenda, EStatusPedido statusPedido)
        {
            var venda = _context.PedidoVendas.Find(idVenda);
            if(venda == null)
                return NotFound();
            
             List<string> statusVenda = new List<string>()
            {
                 "Aguardando_Pagamento",
                 "Pagamento_aprovado",
                 "Enviado_para_transportadora",
                "Entregue",
                 "Cancelada"
            };

            if(statusVenda.IndexOf(venda.StatusPedido.ToString()) == 0 && statusVenda.IndexOf(statusPedido.ToString()) == 1 || statusVenda.IndexOf(venda.StatusPedido.ToString()) == 0 && statusVenda.IndexOf(statusPedido.ToString()) == 4)
            {
                venda.StatusPedido = statusPedido;
            }
            else if(statusVenda.IndexOf(venda.StatusPedido.ToString()) == 1 && statusVenda.IndexOf(statusPedido.ToString()) == 2 || statusVenda.IndexOf(venda.StatusPedido.ToString()) == 1 && statusVenda.IndexOf(statusPedido.ToString()) == 4)
            {
                venda.StatusPedido = statusPedido;
            }
            else if(statusVenda.IndexOf(venda.StatusPedido.ToString()) == 2 && statusVenda.IndexOf(statusPedido.ToString()) == 3)
            {
                venda.StatusPedido = statusPedido;
            }
            else
            {
                throw new Exception("Alteração não permitida");
            }

            _context.SaveChanges();
            return Ok(venda);

        }
       

    }
}