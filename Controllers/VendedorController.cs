using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[Controller]")]
    public class VendedorController : ControllerBase
    {
        private readonly VendasContext _context;

        public VendedorController(VendasContext context)
        {
            _context = context;
        }
        [HttpPost]
        public IActionResult CadastrarVendedor(Vendedor vendedor)
        {

            _context.Add(vendedor);
            _context.SaveChanges();
            return CreatedAtAction(nameof(VendedorPorId), new { idVendedor = vendedor }, vendedor);

        }
        [HttpGet("{idVendedor}")]
        public IActionResult VendedorPorId(int idVendedor)
        {
            var vendedor = _context.Vendedores.Find(idVendedor);
            if (vendedor == null)
                return Ok("Vendedor não encontrado");
            return Ok(vendedor);
        }
    }
}